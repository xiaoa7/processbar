package processbar

import (
	"io"
	"net/http"
	"strconv"
	"testing"
	"time"
)

func TestBar(t *testing.T) {
	bar := NewBar(Bar_Front_Color_CyanineBlue, Show_Length_Mode_Default)
	bar.Reset(100, 0)
	defer bar.Finish()
	for i := 0; i < 50; i++ {
		bar.Inc()
		time.Sleep(time.Second)
	}

}

func TestBar4IO(t *testing.T) {
	url := "https://www.baidu.com/xxx.zip"
	req, _ := http.NewRequest("HEAD", url, nil)
	var contentLength int
	if resp, err := http.DefaultClient.Do(req); err == nil {
		contentLength, _ = strconv.Atoi(resp.Header.Get("Content-Length"))
		resp.Body.Close()
	}
	bar := NewBar(Bar_Front_Color_CyanineBlue, Show_Length_Mode_MByte)
	bar.Reset(int64(contentLength), 0)
	defer bar.Finish()
	req, _ = http.NewRequest("GET", url, nil)
	if resp, err := http.DefaultClient.Do(req); err == nil {
		//io.Copy(io.MultiWriter(buf, bar), resp.Body)
		io.Copy(bar, resp.Body)
		resp.Body.Close()
	}
}
